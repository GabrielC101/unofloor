# Site - Uno Floor

Site desenvolvido para uma empresa no ramo de pisos vinilicos.

> Tecnologias | Technology

- Bootstrap
- HTML5
- CSS3
- JS

> Home

<img src="https://gitlab.com/GabrielC101/unofloor/raw/master/assets/img-preview/home.png">

> Produtos

<img src="https://gitlab.com/GabrielC101/unofloor/raw/master/assets/img-preview/pisos.png">

> Simulador

<img src="https://gitlab.com/GabrielC101/unofloor/raw/master/assets/img-preview/simulador.png">